package partizan.rs.fcpartizan.App;

import android.app.Application;
import android.content.res.Configuration;

import java.util.Locale;

import partizan.rs.fcpartizan.Utilities.Helper;

/**
 * Created by mbibeskovic on 12/7/15.
 */
public class PartizanApp extends Application {
    private Locale locale = null;

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (locale != null) {
            newConfig.locale = locale;
            Locale.setDefault(locale);
            getBaseContext().getResources().updateConfiguration(newConfig, getBaseContext().getResources().getDisplayMetrics());
            getResources().updateConfiguration(newConfig, getResources().getDisplayMetrics());

        }
    }

    @Override
    public void onCreate() {
        super.onCreate();


        Configuration config = getBaseContext().getResources().getConfiguration();

        String lang = "" + Helper.getInstance().getSettedLanguge(this);
        if (!"".equals(lang) && !config.locale.getLanguage().equals(lang)) {
            locale = new Locale(lang);
            Locale.setDefault(locale);
            config.locale = locale;
            getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());

        }
    }
}
