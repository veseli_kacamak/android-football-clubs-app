package partizan.rs.fcpartizan.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiskCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import partizan.rs.fcpartizan.Fragments.FragmentCalendar;
import partizan.rs.fcpartizan.Fragments.FragmentNews;
import partizan.rs.fcpartizan.Fragments.FragmentSocial;
import partizan.rs.fcpartizan.Fragments.FragmentTable;
import partizan.rs.fcpartizan.R;
import partizan.rs.fcpartizan.Utilities.Constants;

public class MainLayoutActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        nostraInit();
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentNews(), getResources().getString(R.string.fragment_news));
        adapter.addFragment(new FragmentTable(), getResources().getString(R.string.fragment_table));
        adapter.addFragment(new FragmentCalendar(), getResources().getString(R.string.fragment_calendar));
        adapter.addFragment(new FragmentSocial(), getResources().getString(R.string.fragment_social));
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent goToDetails = new Intent(MainLayoutActivity.this, ActivityFragmentContainer.class);
            goToDetails.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_SETTINGS);
            startActivity(goToDetails);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void nostraInit() {

        File cacheDir = StorageUtils.getCacheDirectory(MainLayoutActivity.this);

        int MC = (int) ((android.app.ActivityManager) getSystemService(Context.ACTIVITY_SERVICE)).getMemoryClass();
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int maxAloc;
        if (MC >= 48) {
            maxAloc = 3;
        } else {
            maxAloc = 8;
        }
        final double allCacheSize = (maxMemory / maxAloc) * 1000;

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(MainLayoutActivity.this)
                .threadPoolSize(1)
                        // default
                .threadPriority(Thread.NORM_PRIORITY - 1).denyCacheImageMultipleSizesInMemory().memoryCacheSize((int) allCacheSize)
                .memoryCache(new UsingFreqLimitedMemoryCache((int) allCacheSize)).discCache(new UnlimitedDiskCache(cacheDir))
                .discCacheFileNameGenerator(new Md5FileNameGenerator()).tasksProcessingOrder(QueueProcessingType.FIFO).build();

        com.nostra13.universalimageloader.core.ImageLoader.getInstance().init(config);
    }
}
