package partizan.rs.fcpartizan.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import partizan.rs.fcpartizan.Fragments.FragmentNewsContent;
import partizan.rs.fcpartizan.Fragments.FragmentSettings;
import partizan.rs.fcpartizan.R;
import partizan.rs.fcpartizan.Utilities.Constants;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ActivityFragmentContainer extends AppCompatActivity {

    private String newsContent;
    private String newsURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_container_layout);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        /**
         * getting data which is sent to fragment container activity and passing them to fragments
         */
        if (bundle != null) {

            String fragmentName = (String) bundle.get(Constants.FRAGMENT_NAME);

            if (fragmentName.equalsIgnoreCase(Constants.FRAGMENT_NEWS_CONTENT)) {
                newsContent = (String) bundle.get(Constants.NEWS_POST);
                newsURL = (String) bundle.get(Constants.NEWS_URL);

                FragmentNewsContent fragmentSettings = new FragmentNewsContent("" + newsContent, "" + newsURL);
                addMiddle(fragmentSettings, Constants.FRAGMENT_NEWS_CONTENT);
            }
            else if (fragmentName.equalsIgnoreCase(Constants.FRAGMENT_SETTINGS)){
                FragmentSettings fragmentSettings = new FragmentSettings();
                addMiddle(fragmentSettings, Constants.FRAGMENT_SETTINGS);
            }

        }
    }

    /**
     * method for adding fragments to container and back stack
     *
     * @param fragment
     * @param tagAddToBackStack
     */
    private void addMiddle(Fragment fragment, String tagAddToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction tr = fm.beginTransaction();
        tr.replace(R.id.login_container, fragment);
        tr.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            FragmentSettings fragmentSettings = new FragmentSettings();
            addMiddle(fragmentSettings, Constants.FRAGMENT_SETTINGS);
            return true;
        } else if (id == R.id.action_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "" + newsURL);
            sendIntent.setType("text/plain");
            startActivity(Intent.createChooser(sendIntent, ""));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
