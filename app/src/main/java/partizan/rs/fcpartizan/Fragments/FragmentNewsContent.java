package partizan.rs.fcpartizan.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import partizan.rs.fcpartizan.R;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class FragmentNewsContent extends Fragment {

    private String post;
    private String url;
    private WebView wv;
    private View view;


    public FragmentNewsContent() {
    }

    @SuppressLint("ValidFragment")
    public FragmentNewsContent(String post, String url) {
        this.post = post;
        this.url = url;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setHasOptionsMenu(true);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null)
            return null;
        view = inflater.inflate(R.layout.fragment_news_content_layout, container, false);

        wv = (WebView) view.findViewById(R.id.webView);
        wv.getSettings().setDefaultTextEncodingName("utf-8");
        wv.getSettings().setJavaScriptEnabled(true);

        String darkTheme = "background-color: #000000;color: #ffffff;";

        String text = "<html><head>"
                + "<style type=\"text/css\">body {-webkit-text-size-adjust: none;font-size: 14px;font-family: Arial, sans-serif;}"
                + "img {margin: 0 auto;width: 100% !important;height: auto !important;}  " + "alignleft{margin: 0 auto;width: 78% !important;height: auto !important;}"
                + "iframe {margin: 0 auto;width: 100% !important;height: auto !important;}  " + "alignleft{margin: 0 auto;width: 78% !important;height: auto !important;}"
                + "table-type-1{  width:96%; margin-left:2%; !important;height: auto !important; color:#7d7d7d; }"
                + "table{  margin: 0 auto;width: 100% !important;height: auto !important; color:#7d7d7d; } "
                + "table tr:first-child { background-color:#414141 !important; color:#fff !important;} " + "table tr td{ padding-left: 20px;  } "
                + "table tr:nth-child(odd) { background-color: #f8f8f8; } " + "table tr:nth-child(odd) { background-color: #f2f2f2; }"
                + "gallery {margin: 0 auto;width: 20% !important;height: auto !important;}  " + "wp-image{margin: 0 auto;width: 20% !important;height: auto !important;}"
                + "</style></head>" + "<body>" + "<p align=\"justify\">" + post + "</p> " + "</body></html>";
        wv.loadDataWithBaseURL(null, text, "text/html", "utf-8", null);

        return view;

    }

}
