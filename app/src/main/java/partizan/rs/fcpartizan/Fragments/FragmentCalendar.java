package partizan.rs.fcpartizan.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import partizan.rs.fcpartizan.R;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class FragmentCalendar extends Fragment {

    public FragmentCalendar() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar_layout, container, false);
    }
}
