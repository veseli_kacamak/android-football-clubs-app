package partizan.rs.fcpartizan.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import partizan.rs.fcpartizan.Adapters.AdapterNewsList;
import partizan.rs.fcpartizan.Models.ModelNews;
import partizan.rs.fcpartizan.R;
import partizan.rs.fcpartizan.Utilities.ApiCompleteListener;
import partizan.rs.fcpartizan.Utilities.NetworkController;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class FragmentNews extends Fragment {

    private ListView lvNews;
    private View view;
    private AdapterNewsList adapter;
    private SwipeRefreshLayout srlRefresh;

    public FragmentNews() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_news_layout, container, false);
        lvNews = (ListView) view.findViewById(R.id.lvNews);
        srlRefresh = (SwipeRefreshLayout) view.findViewById(R.id.srlRefresh);

        getRecentNews(false);


        srlRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRecentNews(true);
            }
        });


        return view;
    }

    private void getRecentNews(final boolean withRefresh) {

        NetworkController.getIntstance(getActivity()).getRecentNews(new ApiCompleteListener<String>() {
            @Override
            public void onTaskComplete(String result) {

                if (!result.equalsIgnoreCase("")) {

                    if (withRefresh)
                        onItemsLoadComplete();

                    java.lang.reflect.Type collectionType = new TypeToken<ModelNews>() {
                    }.getType();

                    Gson gson = new Gson();
                    ModelNews newsList = gson.fromJson(result, collectionType);

                    if (adapter != null) {
                        adapter.updateAdapter(newsList.getPosts());
                    } else {
                        adapter = new AdapterNewsList(getActivity(), newsList.getPosts());
                    }
                    lvNews.setAdapter(adapter);

                }

            }
        });

    }

    void onItemsLoadComplete() {
        srlRefresh.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (adapter != null){
            adapter.refreshAdapter();
        }
    }
}
