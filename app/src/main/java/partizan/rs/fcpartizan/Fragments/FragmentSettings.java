package partizan.rs.fcpartizan.Fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import java.util.Locale;

import partizan.rs.fcpartizan.R;
import partizan.rs.fcpartizan.Utilities.Constants;
import partizan.rs.fcpartizan.Utilities.Helper;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class FragmentSettings extends Fragment {

    private RadioButton rbBigPictureText;
    private RadioButton rbSmallPictureText;
    private RadioButton rbJustText;
    private RadioButton rbSerbian;
    private RadioButton rbEnglish;
    private View view;

    public FragmentSettings() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_settings_layout, container, false);

        rbBigPictureText = (RadioButton) view.findViewById(R.id.rbBigPictureText);
        rbSmallPictureText = (RadioButton) view.findViewById(R.id.rbSmallPictureText);
        rbJustText = (RadioButton) view.findViewById(R.id.rbJustText);
        rbSerbian = (RadioButton) view.findViewById(R.id.rbSerbian);
        rbEnglish = (RadioButton) view.findViewById(R.id.rbEnglish);

        if (Helper.getInstance().isNewsTypeSelected(getActivity())) {
            if (Helper.getInstance().getNewsLayoutType(getActivity()).equalsIgnoreCase(Constants.NEWS_TYPE_BIG_IMAGE)) {
                rbBigPictureText.setChecked(true);
            } else if (Helper.getInstance().getNewsLayoutType(getActivity()).equalsIgnoreCase(Constants.NEWS_TYPE_SMALL_IMAGE)) {
                rbSmallPictureText.setChecked(true);
            } else {
                rbJustText.setChecked(true);
            }
        } else {
            rbJustText.setChecked(true);
        }

        if (Helper.getInstance().isLanguageSelected(getActivity())) {
            if (Helper.getInstance().getSettedLanguge(getActivity()).equalsIgnoreCase(Constants.SETTINGS_SERBIAN_LANGUAGE))
                rbSerbian.setChecked(true);
            else
                rbEnglish.setChecked(true);
        } else {
            rbEnglish.setChecked(true);
        }

        rbBigPictureText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Helper.getInstance().setIsNewsTypeSelected(true, getActivity());
                if (isChecked)
                    Helper.getInstance().setNewsLayoutType(Constants.NEWS_TYPE_BIG_IMAGE, getActivity());
            }
        });

        rbSmallPictureText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Helper.getInstance().setIsNewsTypeSelected(true, getActivity());
                if (isChecked)
                    Helper.getInstance().setNewsLayoutType(Constants.NEWS_TYPE_SMALL_IMAGE, getActivity());
            }
        });

        rbJustText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Helper.getInstance().setIsNewsTypeSelected(true, getActivity());
                if (isChecked)
                    Helper.getInstance().setNewsLayoutType(Constants.NEWS_TYPE_JUST_TEXT, getActivity());
            }
        });

        rbSerbian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Helper.getInstance().setIsLanguageSelected(true, getActivity());
                if (isChecked)
                    changeLanguage(Constants.SETTINGS_SERBIAN_LANGUAGE);
            }
        });

        rbEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Helper.getInstance().setIsLanguageSelected(true, getActivity());
                if (isChecked)
                    changeLanguage(Constants.SETTINGS_ENGLISH_LANGUAGE);
            }
        });


        return view;
    }

    private void changeLanguage(String language) {


        Helper.getInstance().setLanguage(language, getActivity());

        Locale locale = new Locale(language);
        final Resources resources = getResources();
        Configuration config = resources.getConfiguration();
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(config.locale);
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics());


        Intent i = getActivity().getBaseContext().getPackageManager().getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);


    }
}
