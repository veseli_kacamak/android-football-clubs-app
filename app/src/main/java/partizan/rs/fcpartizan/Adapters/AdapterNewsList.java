package partizan.rs.fcpartizan.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.List;

import partizan.rs.fcpartizan.Activities.ActivityFragmentContainer;
import partizan.rs.fcpartizan.Models.ModelPageContent;
import partizan.rs.fcpartizan.R;
import partizan.rs.fcpartizan.Utilities.Constants;
import partizan.rs.fcpartizan.Utilities.Helper;
import partizan.rs.fcpartizan.Utilities.UIHelper;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class AdapterNewsList extends BaseAdapter implements View.OnClickListener {

    private Activity context;
    private LayoutInflater inflater;
    private List<ModelPageContent> news;
    private static com.nostra13.universalimageloader.core.ImageLoader imageLoader;
    private static DisplayImageOptions options;

    public AdapterNewsList(Activity context, List<ModelPageContent> news) {
        this.inflater = context.getLayoutInflater();
        this.context = context;
        this.news = news;
        configNostra();
    }

    @Override
    public int getCount() {
        if (news != null)
            return news.size();
        else
            return 0;
    }

    @Override
    public ModelPageContent getItem(int position) {
        if (news != null)
            return news.get(position);
        else
            return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        if (Helper.getInstance().isNewsTypeSelected(context)) {
            if (Helper.getInstance().getNewsLayoutType(context).equalsIgnoreCase(Constants.NEWS_TYPE_BIG_IMAGE)) {

                final UIHelper.NewsListItemBigPictureAndText holder = UIHelper.getNewsListItemBigPictureAndText(convertView, inflater, this);

                ModelPageContent pageContent = getItem(position);

                if (pageContent != null) {
                    holder.tvNewsTitle.setText("" + pageContent.getTitle());
                    holder.tvCategoryName.setText("" + pageContent.getCategories().get(0).getTitle());
                    holder.tvDate.setText("" + Helper.getInstance().formateDateFromString("" + pageContent.getDate()));

                    if (pageContent.getAttachments().size() > 0 && pageContent.getAttachments() != null && pageContent.getAttachments().get(0).getImages() != null &&
                            pageContent.getAttachments().get(0).getImages().getFull() != null && pageContent.getAttachments().get(0).getImages().getFull().getUrl() != null) {
                        imageLoader.displayImage(pageContent.getAttachments().get(0).getImages().getFull().getUrl(), holder.ivNewsImage, options);
                    } else {
                        imageLoader.displayImage("drawable://" + R.mipmap.ic_launcher, holder.ivNewsImage, options);
                    }

                    holder.view.setTag(R.id.tag_news_big_image_and_text, position);
                }

                return holder.view;
            }
            if (Helper.getInstance().getNewsLayoutType(context).equalsIgnoreCase(Constants.NEWS_TYPE_SMALL_IMAGE)) {

                final UIHelper.NewsListItemSmallPictureAndText holder = UIHelper.getNewsListItemSmallPictureAndText(convertView, inflater, this);

                ModelPageContent pageContent = getItem(position);

                if (pageContent != null) {

                    holder.tvTitle.setText("" + pageContent.getTitle());
                    holder.tvCategoryName.setText("" + pageContent.getCategories().get(0).getTitle());
                    holder.tvDate.setText("" + Helper.getInstance().formateDateFromString("" + pageContent.getDate()));
                    holder.view.setTag(R.id.tag_news_small_image_and_text, position);

                    if (pageContent.getAttachments().size() > 0 && pageContent.getAttachments() != null && pageContent.getAttachments().get(0).getImages() != null &&
                            pageContent.getAttachments().get(0).getImages().getThumbnail() != null && pageContent.getAttachments().get(0).getImages().getThumbnail().getUrl() != null) {
                        imageLoader.displayImage(pageContent.getAttachments().get(0).getImages().getThumbnail().getUrl(), holder.ivNewsImage, options);
                    } else {
                        imageLoader.displayImage("drawable://" + R.mipmap.ic_launcher, holder.ivNewsImage, options);
                    }


                }
                return holder.view;

            } else {

                final UIHelper.NewsListItemJustText holder = UIHelper.getNewsListItemJustText(convertView, inflater, this);

                ModelPageContent pageContent = getItem(position);

                if (pageContent != null) {

                    holder.tvTitle.setText("" + pageContent.getTitle());
                    holder.tvCategoryName.setText("" + pageContent.getCategories().get(0).getTitle());
                    holder.tvDate.setText("" + Helper.getInstance().formateDateFromString("" + pageContent.getDate()));
                    holder.view.setTag(R.id.tag_news_just_text, position);
                }

                return holder.view;
            }
        } else {
            final UIHelper.NewsListItemJustText holder = UIHelper.getNewsListItemJustText(convertView, inflater, this);

            ModelPageContent pageContent = getItem(position);

            if (pageContent != null) {

                holder.tvTitle.setText("" + pageContent.getTitle());
                holder.tvCategoryName.setText("" + pageContent.getCategories().get(0).getTitle());
                holder.tvDate.setText("" + Helper.getInstance().formateDateFromString("" + pageContent.getDate()));
                holder.view.setTag(R.id.tag_news_just_text, position);
            }

            return holder.view;
        }
    }

    @Override
    public void onClick(View v) {

        Integer tag;

        if (Helper.getInstance().isNewsTypeSelected(context)) {

            if (Helper.getInstance().getNewsLayoutType(context).equalsIgnoreCase(Constants.NEWS_TYPE_BIG_IMAGE))
                tag = (Integer) v.getTag(R.id.tag_news_big_image_and_text);
            else if (Helper.getInstance().getNewsLayoutType(context).equalsIgnoreCase(Constants.NEWS_TYPE_SMALL_IMAGE))
                tag = (Integer) v.getTag(R.id.tag_news_small_image_and_text);
            else
                tag = (Integer) v.getTag(R.id.tag_news_just_text);
        } else
            tag = (Integer) v.getTag(R.id.tag_news_just_text);

        ModelPageContent pageContent = getItem(tag);


        if (pageContent != null) {
            Intent goToDetails = new Intent(context, ActivityFragmentContainer.class);
            goToDetails.putExtra(Constants.FRAGMENT_NAME, Constants.FRAGMENT_NEWS_CONTENT);
            goToDetails.putExtra(Constants.NEWS_POST, pageContent.getContent());
            goToDetails.putExtra(Constants.NEWS_URL, pageContent.getUrl());
            context.startActivity(goToDetails);

        }

    }

    public void updateAdapter(List<ModelPageContent> latesNews) {
        if (latesNews != null) {
            this.news = latesNews;
            notifyDataSetChanged();
        }
    }

    public void refreshAdapter() {
        notifyDataSetChanged();
    }

    public void configNostra() {
        imageLoader = com.nostra13.universalimageloader.core.ImageLoader.getInstance();

        options = new DisplayImageOptions.Builder().bitmapConfig(android.graphics.Bitmap.Config.ARGB_8888).showStubImage(R.mipmap.ic_launcher)
                .showImageForEmptyUri(R.mipmap.ic_launcher).showImageOnFail(R.mipmap.ic_launcher).resetViewBeforeLoading().cacheOnDisc().cacheInMemory().build();

    }
}
