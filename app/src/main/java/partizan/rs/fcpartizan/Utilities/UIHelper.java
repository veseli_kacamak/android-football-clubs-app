package partizan.rs.fcpartizan.Utilities;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import partizan.rs.fcpartizan.R;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class UIHelper {

    public static class NewsListItemBigPictureAndText {
        public ImageView ivNewsImage;
        public TextView tvNewsTitle;
        public TextView tvCategoryName;
        public TextView tvDate;
        public View view;
    }


    public static NewsListItemBigPictureAndText getNewsListItemBigPictureAndText(View convertView, LayoutInflater inflater, View.OnClickListener listener) {
        if (convertView != null && convertView.getTag() != null && convertView.getTag() instanceof NewsListItemBigPictureAndText) {
            return (NewsListItemBigPictureAndText) convertView.getTag();
        }

        convertView = inflater.inflate(R.layout.list_item_news_big_picture_and_text, null);
        NewsListItemBigPictureAndText s = new NewsListItemBigPictureAndText();

        s.ivNewsImage = (ImageView) convertView.findViewById(R.id.ivNewsImage);
        s.tvNewsTitle = (TextView) convertView.findViewById(R.id.tvNewsTitle);
        s.tvCategoryName = (TextView) convertView.findViewById(R.id.tvCategoryName);
        s.tvDate = (TextView) convertView.findViewById(R.id.tvDate);

        convertView.setTag(s);
        s.view = convertView;
        convertView.setTag(R.id.tag_news_big_image_and_text, s);
        convertView.setOnClickListener(listener);
        convertView.setClickable(true);
        return s;
    }


    public static class NewsListItemSmallPictureAndText {
        public ImageView ivNewsImage;
        public TextView tvTitle;
        public TextView tvCategoryName;
        public TextView tvDate;
        public View view;
    }


    public static NewsListItemSmallPictureAndText getNewsListItemSmallPictureAndText(View convertView, LayoutInflater inflater, View.OnClickListener listener) {
        if (convertView != null && convertView.getTag() != null && convertView.getTag() instanceof NewsListItemSmallPictureAndText) {
            return (NewsListItemSmallPictureAndText) convertView.getTag();
        }

        convertView = inflater.inflate(R.layout.list_item_news_small_picture_and_text, null);
        NewsListItemSmallPictureAndText s = new NewsListItemSmallPictureAndText();

        s.ivNewsImage = (ImageView) convertView.findViewById(R.id.ivNewsImage);
        s.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        s.tvCategoryName = (TextView) convertView.findViewById(R.id.tvCategoryName);
        s.tvDate = (TextView) convertView.findViewById(R.id.tvDate);

        convertView.setTag(s);
        s.view = convertView;
        convertView.setTag(R.id.tag_news_small_image_and_text, s);
        convertView.setOnClickListener(listener);
        convertView.setClickable(true);
        return s;
    }


    public static class NewsListItemJustText {
        public TextView tvTitle;
        public TextView tvCategoryName;
        public TextView tvDate;
        public View view;
    }


    public static NewsListItemJustText getNewsListItemJustText(View convertView, LayoutInflater inflater, View.OnClickListener listener) {
        if (convertView != null && convertView.getTag() != null && convertView.getTag() instanceof NewsListItemJustText) {
            return (NewsListItemJustText) convertView.getTag();
        }

        convertView = inflater.inflate(R.layout.list_item_news_just_text, null);
        NewsListItemJustText s = new NewsListItemJustText();

        s.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
        s.tvCategoryName = (TextView) convertView.findViewById(R.id.tvCategoryName);
        s.tvDate = (TextView) convertView.findViewById(R.id.tvDate);

        convertView.setTag(s);
        s.view = convertView;
        convertView.setTag(R.id.tag_news_just_text, s);
        convertView.setOnClickListener(listener);
        convertView.setClickable(true);
        return s;
    }


}
