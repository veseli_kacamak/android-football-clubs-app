package partizan.rs.fcpartizan.Utilities;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public interface ApiCompleteListener<T> {
    public void onTaskComplete(T result);
}
