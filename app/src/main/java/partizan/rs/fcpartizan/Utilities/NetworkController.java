package partizan.rs.fcpartizan.Utilities;

import android.app.Activity;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class NetworkController {

    private static NetworkController intstance;
    private Activity context;

    public NetworkController(Activity context) {
        this.context = context;
    }

    /**
     * getting instance of NetworkController
     *
     * @param context
     * @return
     */
    public static NetworkController getIntstance(Activity context) {
        if (intstance == null) {
            intstance = new NetworkController(context);
        }
        return intstance;
    }


    /**
     *
     * @param callback
     */
    public void getRecentNews(ApiCompleteListener<String> callback) {
        String URL = Constants.API_RECENTS_POST;
        requestGet(callback, URL);
    }

    /**
     * POST REQUEST
     *
     * @param callback
     * @param URL
     */
    public void requestPost(ApiCompleteListener<String> callback, String URL) {
        RequestQueue queue = Volley.newRequestQueue(context);

        Log.d("MILAN_DEBUG", "POST REQUEST URL = " + URL);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                downloadSuccessListener(callback), downloadErrorListener(callback));

        queue.add(stringRequest);
    }


    /**
     * GET REQUEST
     *
     * @param callback
     * @param URL
     */
    public void requestGet(ApiCompleteListener<String> callback, String URL) {
        RequestQueue queue = Volley.newRequestQueue(context);

        Log.d("MILAN_DEBUG", "GET REQUEST URL = " + URL);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                downloadSuccessListener(callback), downloadErrorListener(callback));

        queue.add(stringRequest);
    }

    /**
     * DELETE REQUEST
     *
     * @param callback
     * @param URL
     */
    public void requestDelete(ApiCompleteListener<String> callback, String URL) {
        RequestQueue queue = Volley.newRequestQueue(context);

        Log.d("MILAN_DEBUG", "DELETE REQUEST URL = " + URL);

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, URL,
                downloadSuccessListener(callback), downloadErrorListener(callback));

        queue.add(stringRequest);
    }


    /**
     * PUT REQUEST
     *
     * @param callback
     * @param URL
     */
    public void requestPut(ApiCompleteListener<String> callback, String URL) {
        RequestQueue queue = Volley.newRequestQueue(context);


        Log.d("MILAN_DEBUG", "PUT REQUEST URL = " + URL);

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, URL,
                downloadSuccessListener(callback), downloadErrorListener(callback));

        queue.add(stringRequest);
    }


    /**
     * handling response - successful
     *
     * @param callback
     * @return
     */
    private Response.Listener<String> downloadSuccessListener(final ApiCompleteListener<String> callback) {
        return new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                callback.onTaskComplete(response);
            }

        };
    }

    /**
     * handling response - error
     *
     * @param callback
     * @return
     */
    private Response.ErrorListener downloadErrorListener(final ApiCompleteListener<String> callback) {
        return new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onTaskComplete("ERROR = " + error.toString());
            }
        };
    }


}
