package partizan.rs.fcpartizan.Utilities;

/**
 * Created by mbibeskovic on 12/5/15.
 */
public class Prefs {

    /**
     * settings prefs
     */
    public static final String NEWS_TYPE = "NEWS_TYPE";
    public static final String IS_NEWS_TYPE_SELECTED = "IS_NEWS_TYPE_SELECTED";
    public static final String NEWS_TYPE_SET = "NEWS_TYPE_SET";
    public static final String NEWS_TYPE_SELECTED = "NEWS_TYPE_SELECTED";
    public static final String IS_THEME_SELECTED = "IS_THEME_SELECTED";
    public static final String IS_LANGUAGE_SELECTED = "IS_LANGUAGE_SELECTED";
    public static final String IS_LANGUAGE_SELECTED_FLAG = "IS_LANGUAGE_SELECTED_MARK";
    public static final String SETTED_LANGUAGE_FLAG = "SETTED_LANGUAGE_FLAG";
    public static final String SETTED_LANGUAGE_SELECTED = "SETTED_LANGUAGE_SELECTED";


}
