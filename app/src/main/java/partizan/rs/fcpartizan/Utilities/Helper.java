package partizan.rs.fcpartizan.Utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.view.Display;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class Helper {

    private static Helper instance;

    public Helper() {
    }

    /**
     * getting instance of Helper class
     *
     * @return
     */
    public static Helper getInstance() {
        if (instance == null) {
            instance = new Helper();
        }
        return instance;
    }


    public String formateDateFromString(String date) {

        String reformattedStr = "";
        SimpleDateFormat fromUser;
        fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat myFormat = new SimpleDateFormat("dd.MM.yyyy");

        try {

            reformattedStr = myFormat.format(fromUser.parse(date));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return reformattedStr;
    }


    /**
     * getting app screen width
     *
     * @param context
     * @return
     */
    public int getAppScreenWidth(Activity context) {

        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;


        return width;
    }

    /**
     * getting app screen height
     *
     * @param context
     * @return
     */
    public int getAppScreenHeight(Activity context) {

        Display display = context.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int height = size.y;


        return height;
    }


    /**
     * set is news type selected
     *
     * @param is
     * @param context
     */
    public void setIsNewsTypeSelected(boolean is, Activity context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Prefs.NEWS_TYPE, 0).edit();
        editor.putBoolean(Prefs.IS_NEWS_TYPE_SELECTED, is);
        editor.apply();
        editor.commit();
    }


    /**
     * is news type selected
     *
     * @param context
     * @return
     */
    public boolean isNewsTypeSelected(Activity context) {
        return context.getSharedPreferences(Prefs.NEWS_TYPE, 0).getBoolean(Prefs.IS_NEWS_TYPE_SELECTED, false);
    }

    /**
     * set news type
     *
     * @param type
     * @param context
     */
    public void setNewsLayoutType(String type, Activity context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Prefs.NEWS_TYPE_SET, 0).edit();
        editor.putString(Prefs.NEWS_TYPE_SELECTED, type);
        editor.apply();
        editor.commit();
    }


    /**
     * get news type
     *
     * @param context
     * @return
     */
    public String getNewsLayoutType(Activity context) {
        return context.getSharedPreferences(Prefs.NEWS_TYPE_SET, 0).getString(Prefs.NEWS_TYPE_SELECTED, null);
    }



    public void setLanguage(String language, Activity context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Prefs.SETTED_LANGUAGE_SELECTED, 0).edit();
        editor.putString(Prefs.SETTED_LANGUAGE_SELECTED, language);
        editor.apply();
        editor.commit();
    }


    /**
     * get news type
     *
     * @param context
     * @return
     */
    public String getSettedLanguge(Context context) {
        return context.getSharedPreferences(Prefs.SETTED_LANGUAGE_SELECTED, 0).getString(Prefs.SETTED_LANGUAGE_SELECTED, null);
    }

    public void setIsLanguageSelected(boolean selected, Activity context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(Prefs.IS_LANGUAGE_SELECTED, 0).edit();
        editor.putBoolean(Prefs.IS_LANGUAGE_SELECTED, selected);
        editor.apply();
        editor.commit();
    }


    /**
     * get news type
     *
     * @param context
     * @return
     */
    public boolean isLanguageSelected(Context context) {
        return context.getSharedPreferences(Prefs.IS_LANGUAGE_SELECTED, 0).getBoolean(Prefs.IS_LANGUAGE_SELECTED, false);
    }

}
