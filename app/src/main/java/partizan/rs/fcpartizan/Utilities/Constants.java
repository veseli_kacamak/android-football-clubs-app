package partizan.rs.fcpartizan.Utilities;

/**
 * Created by mbibeskovic on 12/1/15.
 */
public class Constants {

    /**
     * CONSTANTS
     */
    public static final String BASE_URL = "http://api.s1.code-track.com";
    public static final String SERVER_PARAMS = "&kjasdhuo32032njfdnasp1=j3023ucmrwnur92nrnh3gxjc";
    public static final String TEST_PROJECT_CODE = "Ct83uZRN";
    public static final String TEST_USER_ID = "1";
    public static final String TEST_SERVER_no = "server_1";
    public static final String TEST_DELETE_PHRASE = "DELETE THIS PROJECT";

    /**
     * REQUESTS
     */
    public static final String GET = "/GET";
    public static final String PUT = "/PUT";
    public static final String POST = "/POST";
    public static final String DELETE = "/DELETE";


    /**
     * API CALLS
     */

    public static final String API_BASE_URL_ENG = "http://www.en.partizan.rs/api/";
    public static final String API_PAGE = API_BASE_URL_ENG + "get_page/?page_slug=";
    public static final String API_POST = API_BASE_URL_ENG + "get_post/?post_id=";
    public static final String API_RECENTS_POST = API_BASE_URL_ENG + "get_recent_posts/";
    public static final String API_TABLE_JELEN_SUPERLIGA = API_PAGE + "tabela-jelen-super-liga-201314";
    public static final String API_TABLE_UEFA = API_PAGE + "uefa-takmicenja";

    /**
     * NEWS
     */

    public static final String NEWS_POST = "NEWS_POST";
    public static final String NEWS_URL = "NEWS_URL";
    public static final String FRAGMENT_NAME = "FRAGMENT_NAME";


    /**
     * FRAGMENT NAMES
     */
    public static final String FRAGMENT_NEWS_CONTENT = "NEWS_CONTENT";
    public static final String FRAGMENT_SETTINGS = "SETTINGS";


    /**
     * SETTINGS
     */

    public static final String NEWS_TYPE_BIG_IMAGE = "NEWS_TYPE_BIG_IMAGE";
    public static final String NEWS_TYPE_SMALL_IMAGE = "NEWS_TYPE_SMALL_IMAGE";
    public static final String NEWS_TYPE_JUST_TEXT = "NEWS_TYPE_JUST_TEXT";
    public static final String SETTINGS_SERBIAN_LANGUAGE = "sr";
    public static final String SETTINGS_ENGLISH_LANGUAGE = "en";




}
