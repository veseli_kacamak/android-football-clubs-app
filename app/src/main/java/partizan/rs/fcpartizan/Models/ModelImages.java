package partizan.rs.fcpartizan.Models;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelImages {

    private ModelFullImage full;
    private ModelThumnailImage thumbnail;
    private ModelMediumImage medium;
    private ModelLargeImage large;


    public ModelFullImage getFull() {
        return full;
    }

    public void setFull(ModelFullImage full) {
        this.full = full;
    }

    public ModelThumnailImage getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ModelThumnailImage thumbnail) {
        this.thumbnail = thumbnail;
    }

    public ModelMediumImage getMedium() {
        return medium;
    }

    public void setMedium(ModelMediumImage medium) {
        this.medium = medium;
    }

    public ModelLargeImage getLarge() {
        return large;
    }

    public void setLarge(ModelLargeImage large) {
        this.large = large;
    }
}
