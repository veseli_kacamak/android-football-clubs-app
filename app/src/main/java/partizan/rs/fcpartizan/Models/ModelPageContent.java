package partizan.rs.fcpartizan.Models;

import java.util.List;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelPageContent {

    private String id;
    private String type;
    private String url;
    private String status;
    private String title;
    private String content;
    private String excerpt;
    private String date;
    private String modified;
    public List<ModelCategories> categories;
    private ModelAuthor author;
    private List<ModelAttachments> attachments;
    private String thumbnail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public List<ModelCategories> getCategories() {
        return categories;
    }

    public void setCategories(List<ModelCategories> categories) {
        this.categories = categories;
    }

    public ModelAuthor getAuthor() {
        return author;
    }

    public void setAuthor(ModelAuthor author) {
        this.author = author;
    }

    public List<ModelAttachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<ModelAttachments> attachments) {
        this.attachments = attachments;
    }
}
