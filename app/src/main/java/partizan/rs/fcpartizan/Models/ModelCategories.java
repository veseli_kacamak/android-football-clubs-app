package partizan.rs.fcpartizan.Models;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelCategories {

    private int id;
    private String slug;
    private String title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
