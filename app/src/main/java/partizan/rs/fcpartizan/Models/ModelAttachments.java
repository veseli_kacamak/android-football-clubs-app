package partizan.rs.fcpartizan.Models;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelAttachments {

    private String id;
    private String url;
    private String title;
    private String mime_type;
    private ModelImages images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMime_type() {
        return mime_type;
    }

    public void setMime_type(String mime_type) {
        this.mime_type = mime_type;
    }

    public ModelImages getImages() {
        return images;
    }

    public void setImages(ModelImages images) {
        this.images = images;
    }


}
