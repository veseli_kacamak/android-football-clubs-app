package partizan.rs.fcpartizan.Models;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelThumnailImage {

    private String url;
    private String width;
    private String height;


    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public String getWidth() {
        return width;
    }
    public void setWidth(String width) {
        this.width = width;
    }
    public String getHeight() {
        return height;
    }
    public void setHeight(String height) {
        this.height = height;
    }

}
