package partizan.rs.fcpartizan.Models;

import java.util.List;

/**
 * Created by mbibeskovic on 12/2/15.
 */
public class ModelNews {

    private String count;
    private List<ModelPageContent> posts;


    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public List<ModelPageContent> getPosts() {
        return posts;
    }

    public void setPosts(List<ModelPageContent> posts) {
        this.posts = posts;
    }
}
